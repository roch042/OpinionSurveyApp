make_data <- function() {
  
  require(tidyverse)
  
  # read in pronghorn data ####
  DAT <- readr::read_csv("data/crowd20/idfg_crowd_rg_2020_WORK.csv",col_types=cols(.default="c"))
  DAT_HuntDays <- readr::read_csv("data/crowd20/idfg_crowd_rg_2020_WORK_huntdays.csv",col_types=cols(.default="c"))
  survey_name <- "crowd20_rg"
  
  # read in question translation ####
  # - create ValuePlot and recode for readability
  dt_translation <- readr::read_csv("data/crowd20/cdbk_idfg_crowd_20_rg.csv") %>%
    dplyr::mutate(ValuePlot = dplyr::recode(Value,
                                            "Trail designations that limited access (for example, non-motorized vehicle trails)"="Trail designations that limited access",
                                            "Environmental conditions (for example, severe weather)"="Environmental conditions",
                                            "Personal issues (for example, physical conditioning)"="Personal issues",
                                            "Black (or African American)"="Black",
                                            "Hispanic (or Latino)"="Hispanic",
                                            "Indigenous (Native American or Native Alaskan)"="Indigenous",
                                            "Native Hawiian (or other Pacific Islander)"="Native Hawiian",
                                            "White (or Caucasian)"="White"
    ))
  
  # format DAT_HuntDays ####
  nvars <- DAT_HuntDays$Q11_HuntDays %>%
    stringr::str_count(pattern = ",") %>%
    max(., na.rm=T) + 1
  
  DAT_HuntDays <- DAT_HuntDays %>%
    dplyr::group_by(ID, Q11a_HuntDays_Count) %>%
    tidyr::separate(Q11_HuntDays, into = paste0("col_",1:nvars), sep = ", ") %>%
    tidyr::pivot_longer(cols = dplyr::starts_with("col_"), names_to = "name", values_to = "Q11_HuntDays") %>%
    dplyr::filter(!is.na(Q11_HuntDays)) %>%
    dplyr::select(-name)
  
  # Add the DAT_HuntDays to DAT
  DAT <- DAT %>%
    dplyr::left_join(DAT_HuntDays)
  
  # Replace all -99 with NA
  DAT <- DAT %>%
    dplyr::mutate_all(~ replace(., . == -99 |. == "-99", NA))
  
  # Change Q32_Age and Q33_Resident to actual number of years (not the year of birth and year they became a resident), calculate from year of survey (2020)
  DAT <- DAT %>%
    dplyr::mutate(Q32_Age = 2020 - as.numeric(as.character(Q32_Age)),
                  Q33_Resident = 2020 - as.numeric(as.character(Q33_Resident)))
  
  # Add AgeCat
  DAT <- DAT %>%
    dplyr::mutate(AgeCat = dplyr::case_when(
      (Q32_Age >= 18 & Q32_Age <= 24) ~ "1",
      (Q32_Age >= 25 & Q32_Age <= 34) ~ "2",
      (Q32_Age >= 35 & Q32_Age <= 44) ~ "3",
      (Q32_Age >= 45 & Q32_Age <= 54) ~ "4",
      (Q32_Age >= 55 & Q32_Age <= 64) ~ "5",
      (Q32_Age >= 65 & Q32_Age <= 74) ~ "6",
      (Q32_Age >= 75) ~ "7"
    ))
  
  DAT <- DAT %>%
    dplyr::mutate(Q32_Age = as.character(Q32_Age),
                  Q33_Resident = as.character(Q33_Resident))
  
  # exclude 'Name' you don't want as questions
  dt_summary_group_choices <- dt_translation %>%
    dplyr::select(Name, Label, Question_Number) %>%
    dplyr::distinct() %>%
    dplyr::filter(!Name %in% c("ID","Region","Zip","Q11_HuntDays","Q11a_HuntDays_Count","Start","End","Progress","Duration","Finish","Lat","Lon","SportID","Weight_State","Weight_Strata")) %>%
    dplyr::mutate(NewLabel = ifelse(!is.na(Question_Number), paste0("(",Question_Number,") ",Label), Label))
  
  # list the prefix to dt_translation$Name that should be included for taking the mean
  Q_means <- c("Q8a","Q11","Q13","Q14","Q15","Q16","Q18","Q19","Q20","Q23","Q24","Q25","Q26","Q27","Q28","Q29","Q30","Q31","Q32","Q33")
  
  # complete the dt_translation for answers that didn't have Code/Value/ValuePlot
  col_sel <- c("Q32_Age", "Q33_Resident")
  
  value_index <- DAT %>%
    dplyr::select(dplyr::one_of(col_sel)) %>%
    tidyr::pivot_longer(cols = dplyr::one_of(col_sel)) %>%
    dplyr::distinct() %>%
    dplyr::arrange(name, value) %>%
    dplyr::filter(!is.na(value))
  
  dt_translation <- dt_translation %>%
    dplyr::left_join(value_index, by = c("Name"="name")) %>%
    dplyr::mutate(Value = dplyr::if_else(!is.na(value), value, Value),
                  ValuePlot = dplyr::if_else(!is.na(value), value, ValuePlot),
                  Code = dplyr::if_else(!is.na(value), value, Code)) %>%
    dplyr::mutate(Qmean = stringr::str_detect(Name, paste0(Q_means,collapse="|"))) %>%
    dplyr::mutate(Survey = survey_name) %>%
    dplyr::select(-value)
  
  # remove Hunt Days
  DAT <- DAT %>%
    dplyr::select(-Q11a_HuntDays_Count,-Q11_HuntDays)
  
  # Data in long form
  DAT_long <- DAT %>%
    tidyr::pivot_longer(cols = -c(ID)) %>%
    dplyr::mutate(Survey = survey_name) %>%
    dplyr::distinct()
  
  return(list(
    survey_id = "ID",
    dt_translation = dt_translation,
    DAT = DAT,
    DAT_long = DAT_long,
    Q_means = Q_means,
    dt_summary_group_choices = dt_summary_group_choices
  ))
  
}