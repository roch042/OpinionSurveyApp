server_datatrans_module <- function(id, dat, sidepanel_selections, Q_means, calc_mean) {
  
  moduleServer(
    
    id,
    
    function(input, output, session){
      
      # __dat_trans_prong21 ####
      dat_trans <- reactive({

        req(sidepanel_selections$group1())
        
        xstar <- fnc_dat_trans(
          dt_translation = dt_translation,
          dat = dat,
          group1 = sidepanel_selections$group1(),
          group1_filter = sidepanel_selections$group1_filter(),
          group2 = ifelse(!isTruthy(sidepanel_selections$group2()), NA, sidepanel_selections$group2()),
          group2_filter = sidepanel_selections$group2_filter(),
          group3 = ifelse(!isTruthy(sidepanel_selections$group3()), NA, sidepanel_selections$group3()),
          group3_filter = sidepanel_selections$group3_filter(),
          Q_means = Q_means,
          calc_mean = calc_mean
        )

        return(xstar)
        
      })

      return(dat_trans)
      
    }
  )
  
}