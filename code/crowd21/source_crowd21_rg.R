make_data <- function() {
  
  require(tidyverse)
  
  # read in pronghorn data ####
  DAT <- readr::read_csv("data/crowd21/idfg_crowd_rg_2021_WORK.csv",col_types=cols(.default="c"))
  survey_name <- "crowd21_rg"
  
  # split out the hunt day information ####
  DAT_HuntDays <- DAT %>%
    dplyr::select(ID, Q5_Day, Q6_Day2)
  
  DAT <- DAT %>%
    dplyr::select(-Q5_Day, -Q6_Day2)
  
  # read in question translation ####
  # - create ValuePlot and recode for readability
  dt_translation <- readr::read_csv("data/crowd21/cdbk_idfg_crowd_21_WORK_rg.csv") %>%
    dplyr::mutate(ValuePlot = dplyr::recode(Value,
                                            "Much worse (I encountered many more hunters than I expected)"="Much worse",
                                            "Somewhat worse (I encountered more hunters than I expected)"="Somewhat worse",
                                            "About the same (I encountered the number of other hunters I expected)"="About the same",
                                            "Somewhat better (I encountered fewer hunters than I expected)"="Somewhat better",
                                            "Much better (I encountered far fewer hunters than I expected)"="Much better",
                                            "Black (or African American)"="Black",
                                            "Hispanic (or Latino)"="Hispanic",
                                            "Indigenous (Native American or Native Alaskan)"="Indigenous",
                                            "Native Hawiian (or other Pacific Islander)"="Native Hawiian",
                                            "White (or Caucasian)"="White"
    ))
  
  # format DAT_HuntDays ####
  nvars <- DAT_HuntDays$Q5_Day %>%
    stringr::str_count(pattern = ",") %>%
    max(., na.rm=T) + 1
  
  DAT_HuntDays <- DAT_HuntDays %>%
    dplyr::group_by(ID, Q6_Day2) %>%
    tidyr::separate(Q5_Day, into = paste0("col_",1:nvars), sep = ", ") %>%
    tidyr::pivot_longer(cols = dplyr::starts_with("col_"), names_to = "name", values_to = "Q5_Day") %>%
    dplyr::filter(!is.na(Q5_Day)) %>%
    dplyr::select(-name)
  
  # Add the DAT_HuntDays to DAT
  DAT <- DAT %>%
    dplyr::left_join(DAT_HuntDays)
  
  # Replace all -99 with NA
  DAT <- DAT %>%
    dplyr::mutate_all(~ replace(., . == -99 |. == "-99", NA))
  
  
  # exclude 'Name' you don't want as questions
  dt_summary_group_choices <- dt_translation %>%
    dplyr::select(Name, Label, Question_Number) %>%
    dplyr::distinct() %>%
    dplyr::filter(!Name %in% c("ID","Region","Wght_Age_State","Inclusion1","Inclusion2","Progress","Duration_minutes","Duration_seconds","Q5_Day","Q6_Day2") & !grepl("SD_",Name)) %>%
    dplyr::mutate(NewLabel = ifelse(!is.na(Question_Number), paste0("(",Question_Number,") ",Label), Label))
  
  # list the prefix to dt_translation$Name that should be included for taking the mean
  Q_means <- c("Q5","Q6","Q8","Q9","Q10","Q11","Q12","Q14","Q15","Q16","Q17","Q18","Q19","Q20","Q21","Q22","Q23","Q24","Q25","Q26","Age","Residency")
  
  # complete the dt_translation for answers that didn't have Code/Value/ValuePlot
  col_sel <- c("Residency","Age")
  
  value_index <- DAT %>%
    dplyr::select(dplyr::one_of(col_sel)) %>%
    tidyr::pivot_longer(cols = dplyr::one_of(col_sel)) %>%
    dplyr::distinct() %>%
    dplyr::arrange(name, value) %>%
    dplyr::filter(!is.na(value))
  
  dt_translation <- dt_translation %>%
    dplyr::left_join(value_index, by = c("Name"="name")) %>%
    dplyr::mutate(Value = dplyr::if_else(!is.na(value), value, Value),
                  ValuePlot = dplyr::if_else(!is.na(value), value, ValuePlot),
                  Code = dplyr::if_else(!is.na(value), value, Code)) %>%
    dplyr::mutate(Qmean = stringr::str_detect(Name, paste0(Q_means,collapse="|"))) %>%
    dplyr::mutate(Survey = survey_name) %>%
    dplyr::select(-value)
  
  # remove Hunt Days
  DAT <- DAT %>%
    dplyr::select(-Q5_Day,-Q6_Day2)
  
  # Data in long form
  DAT_long <- DAT %>%
    tidyr::pivot_longer(cols = -c(ID)) %>%
    dplyr::mutate(Survey = survey_name) %>%
    dplyr::distinct()
  
  return(list(
    survey_id = "ID",
    dt_translation = dt_translation,
    DAT = DAT,
    DAT_long = DAT_long,
    Q_means = Q_means,
    dt_summary_group_choices = dt_summary_group_choices
  ))
  
}